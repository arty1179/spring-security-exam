package com.panasyuk.secure.service;

import com.panasyuk.secure.entity.Role;
import com.panasyuk.secure.entity.User;
import com.panasyuk.secure.entity.dto.UserDTO;
import com.panasyuk.secure.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public User findUserById(@Nullable final String userId) {
        if (userId == null) {
            return null;
        }
        @NotNull final Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(null);
    }

    @Override
    public List<User> allUsers() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public boolean saveUser(@Nullable final User user) {
        if (user == null) {
            return false;
        }
        @Nullable final User userFromDB = userRepository.findById(user.getId()).orElse(null);
        if (userFromDB != null) {
            return false;
        }
        @NotNull final List<Role> roles = new ArrayList<>();
        roles.add(new Role(user));
        user.setRoles(roles);
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    @Override
    public boolean deleteUser(@Nullable final String userId) {
        if (userId == null) {
            return false;
        }
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    @Override
    public User findByUsername(@Nullable final String username) {
        return userRepository.findByUsername(username);
    }

    @Nullable
    public User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @Nullable final User user = new User();
        user.setId(userDTO.getId());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        return user;
    }

    @Nullable
    public UserDTO toUserDTO(@Nullable final User user) {
        if (user == null) return null;
        @Nullable final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }
}
