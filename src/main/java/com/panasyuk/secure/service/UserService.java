package com.panasyuk.secure.service;

import com.panasyuk.secure.entity.User;
import com.panasyuk.secure.entity.dto.UserDTO;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface UserService {

    @Nullable
    User findUserById(@Nullable final String userId);

    @Nullable
    List<User> allUsers();

    boolean saveUser(@Nullable final User user);

    boolean deleteUser(@Nullable final String userId);

    User findByUsername(@Nullable final String username);

    @Nullable
    User toUser(@Nullable final UserDTO userDTO);

    @Nullable
    UserDTO toUserDTO(@Nullable final User user);

}
