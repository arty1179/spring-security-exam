package com.panasyuk.secure.entity.dto;

import com.panasyuk.secure.entity.enums.RoleType;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public class UserDTO {

    @Setter
    @Getter
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Setter
    @Getter
    @Nullable
    private String username;

    @Setter
    @Getter
    @Nullable
    private String password;

    @NotNull
    private RoleType role = RoleType.USER;

}
