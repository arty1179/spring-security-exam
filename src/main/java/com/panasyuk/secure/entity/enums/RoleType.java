package com.panasyuk.secure.entity.enums;

public enum RoleType {
    USER,
    ADMIN;
}
