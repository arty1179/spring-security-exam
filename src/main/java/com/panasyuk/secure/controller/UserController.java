package com.panasyuk.secure.controller;

import com.panasyuk.secure.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/admin")
    @Secured({"ROLE_ADMIN"})
    public String userList(@NotNull final Model model) {
        model.addAttribute("allUsers", userService.allUsers());
        return "admin";
    }

    @PostMapping("/admin/delete")
    @Secured({"ROLE_ADMIN"})
    public String deleteUser(@NotNull @RequestParam final String userId,
                              @NotNull @RequestParam final String action,
                              @NotNull final Model model) {
        if (action.equals("delete")){
            userService.deleteUser(userId);
        }
        return "redirect:/admin";
    }

    @GetMapping("/admin/get/{userId}")
    @Secured({"ROLE_ADMIN"})
    public String  getUser(@NotNull @PathVariable("userId") final String userId,
                           @NotNull final Model model) {
        model.addAttribute("user", userService.findUserById(userId));
        return "admin";
    }

}
