<%@ page language="java" pageEncoding="UTF-8" %>

<h1>Список пользователей</h1>

<table  class="table table-striped">
    <col width="50px">
    <col>
    <col>
    <col width="20px">
    <col width="20px">
    <tr>
        <th>ID</th>
        <th>Login</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    <c:forEach var="user" items="${allUsers}">
        <tr>
            <td><c:out value="${user.id}"/></td>
            <td><c:out value="${user.username}"/></td>
        </tr>
    </c:forEach>
</table>